from django.apps import AppConfig


class DjangodockerConfig(AppConfig):
    name = 'djangodocker'
